output "resource_group_id" {
  description = "Resource Group ID"
  value = azurerm_resource_group.example.id
}

output "resource_group_name" {
  description = "Resource Group ID"
  value = azurerm_resource_group.example.name
}

#thre vm we have created
output "vm_ip_information" {
  description = "display ip of vm"
  value = [for vm in azurerm_linux_virtual_machine.web_linuxvm: vm.private_ip_addresses]
}
output "lb-public-ip" {
  description = "load balancer public ip"
  value = azurerm_public_ip.web_lb_public_ip.ip_address
}