#first we will create the public ip of the load balancer
resource "azurerm_public_ip" "web_lb_public_ip" {
  name                = "${local.resource_name_prefix}-lbpublicip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Static"
  sku = "Standard"

  tags = local.common_tags
}

#next we will create the load balancer
resource "azurerm_lb" "web_lb" {
 name                = "${local.resource_name_prefix}-weblb"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  sku = "Standard" #Premium they will deploy the lb in two different region
#sku_tier = "Global"
  frontend_ip_configuration {
    name                 = "lb"
    public_ip_address_id = azurerm_public_ip.web_lb_public_ip.id
  }
}

#backend address pool
resource "azurerm_lb_backend_address_pool" "web_lb_pool" {
  name =  "${local.resource_name_prefix}-backendpool"
  #this backend pool will be part of your lb
  loadbalancer_id = azurerm_lb.web_lb.id
}

#we will create pobes 
resource "azurerm_lb_probe" "web_lb_probe" {
  name = "${local.resource_name_prefix}-tcpprobe"
  protocol = "Tcp"
  port = 80
  loadbalancer_id = azurerm_lb.web_lb.id
}

#we will create lb rule
resource "azurerm_lb_rule" "web_lb_rule" {
  name = "${local.resource_name_prefix}-lbrule"
  protocol = "Tcp"
  frontend_port = 80
  backend_port = 80 #
  frontend_ip_configuration_name = azurerm_lb.web_lb.frontend_ip_configuration[0].name
 backend_address_pool_ids = [azurerm_lb_backend_address_pool.web_lb_pool.id]
  probe_id = azurerm_lb_probe.web_lb_probe.id 
  loadbalancer_id = azurerm_lb.web_lb.id 
}

#in the backend address pool we need to attach those vm 
resource "azurerm_network_interface_backend_address_pool_association" "web_lb_association" {
  for_each = var.force_map
  network_interface_id = azurerm_network_interface.web_linux_nic[each.key].id
  ip_configuration_name = azurerm_network_interface.web_linux_nic[each.key].ip_configuration[0].name 
  backend_address_pool_id = azurerm_lb_backend_address_pool.web_lb_pool.id
}