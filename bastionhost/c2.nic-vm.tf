resource "azurerm_network_interface" "web_linux_nic" {
  name                = "${local.resource_name_prefix}-nic"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  #we are creating nic this nic will also have public
  ip_configuration {
    name = "web-linux-ip"
    #nic will be part of your subnet
    subnet_id = azurerm_subnet.web-subnet.id 
    private_ip_address_allocation = "Dynamic"
   # public_ip_address_id = azurerm_public_ip.public-ip.id
  }
}