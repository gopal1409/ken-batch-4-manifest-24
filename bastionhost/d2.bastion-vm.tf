resource "azurerm_public_ip" "public-ip" {
  name                = "${local.resource_name_prefix}-publicip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Static"
  sku = "Standard"

  tags = local.common_tags
}

resource "azurerm_network_interface" "bastion_linux_nic" {
  name                = "${local.resource_name_prefix}-bastion-nic"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  #we are creating nic this nic will also have public
  ip_configuration {
    name = "bastion-ip"
    #nic will be part of your subnet
    subnet_id = azurerm_subnet.bastion-subnet.id  
    private_ip_address_allocation = "Dynamic"
   public_ip_address_id = azurerm_public_ip.public-ip.id
  }
}
resource "azurerm_linux_virtual_machine" "bastion_linuxvm" {
    name                = "${local.resource_name_prefix}-bastion-vm"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F2"
  admin_username      = "azureuser"
  #admin_password = "Admin@12345678"
  #disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.bastion_linux_nic.id,
  ]

  admin_ssh_key {
    username   = "azureuser"
    public_key = file("${path.module}/ssh-key/terraform-azure.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
  ##to run an cuistom script startup script
  #the purpose is to look for the file in current directory
 # custom_data = filebase64("${path.module}/app/app.sh")
 #while provisioing the jump box i want to harden the vm
}