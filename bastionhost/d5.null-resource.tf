resource "null_resource" "null_copy_ssh" {
  depends_on = [ azurerm_linux_virtual_machine.bastion_linuxvm ]
  #so that the connection will not be execute till the vm is fully provison
  connection {
    type = "ssh" #RDP
    host = azurerm_linux_virtual_machine.bastion_linuxvm.public_ip_address
    user =  azurerm_linux_virtual_machine.bastion_linuxvm.admin_username
    private_key = file("${path.module}/ssh-key/terraform-azure.pem")
   }

   #once the connection is being made  we need to upload the pem file inside the bastion host
   provisioner "file" {
     source = "ssh-key/terraform-azure.pem" #current directory
     destination = "/tmp/terraform-azure.pem" #in the host machine temp directory
   }
   #once we upload i want to execute that file also. 
   provisioner "remote-exec" {
     inline = [ 
        "sudo chmod 400 /tmp/terraform-azure.pem"
        #java -jar agent.jar
        #it will make read only for the ownere
      ]
   }
}