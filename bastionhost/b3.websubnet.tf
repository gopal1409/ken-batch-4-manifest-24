###created web subnet
resource "azurerm_subnet" "web-subnet" {
  name                 = "${local.resource_name_prefix}-${var.web_subnet_name}"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.web_subnet_address
}

##lets create the nsg
resource "azurerm_network_security_group" "web_subnet_nsg" {
  name                 = "${local.resource_name_prefix}-nsg"
  resource_group_name  = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
}

#this nsg need to be part of your subnet also so i need to associate the nsg with subnet 
resource "azurerm_subnet_network_security_group_association" "web_subnet_nsg" {
    depends_on = [ azurerm_network_security_rule.web_nsg_rule_inbound ]
  subnet_id = azurerm_subnet.web-subnet.id 
  network_security_group_id = azurerm_network_security_group.web_subnet_nsg.id
}
#why we use local against a name we can define expression key value format
locals {
  web_inbound_port = {
    "110" : "80",
    "120" : "22",
    "130" : "443"
  }
}
#we will create nsg rule
###did you find any problem in the rule
resource "azurerm_network_security_rule" "web_nsg_rule_inbound" {
for_each = local.web_inbound_port
  name                        = "Rule-port-${each.value}" #80
  priority                    = each.key #110
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*" 
  destination_port_range      = each.value
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.example.name
  network_security_group_name = azurerm_network_security_group.web_subnet_nsg.name
}

