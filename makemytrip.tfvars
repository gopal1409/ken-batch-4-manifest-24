resource_group_name = "rg-trip"
resource_group_location="eastus"
business_devision="trip"
environment="prod"
vnet_name = "trip"
vnet_address_space = ["10.1.0.0/16"]
web_subnet_name = "tripsubnet"
web_subnet_address = ["10.1.1.0/24"]
force_map = {
    "han" = "solo",
      "rey" = "jedi"
      
}